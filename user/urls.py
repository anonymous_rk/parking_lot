from django.urls import path
from .views import register, CustomUserLoginView, logout_view
from .api_urls import urlpatterns as api_urlpatterns

urlpatterns = [
    path('register/', register, name='register'),
    path('login/', CustomUserLoginView.as_view(), name='login'),
    path('logout/', logout_view, name='logout'),
] + api_urlpatterns
