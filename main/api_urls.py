from django.urls import path
from .api import ParkingSpaceListAPI, ParkingSpaceCreateAPI, ParkingSpaceUpdateAPI, ParkingSpaceDeleteAPI, \
                 ReservationListAPI, ReservationCreateAPI, ReservationUpdateAPI, ReservationDeleteAPI


urlpatterns = [
    path('parking-space-list/', ParkingSpaceListAPI.as_view()),
    path('parking-space-create/', ParkingSpaceCreateAPI.as_view()),
    path('parking-space-update/<int:pk>/', ParkingSpaceUpdateAPI.as_view()),
    path('parking-space-delete/<int:pk>/', ParkingSpaceDeleteAPI.as_view()),
    path('reservation-list/', ReservationListAPI.as_view()),
    path('reservation-create/', ReservationCreateAPI.as_view()),
    path('reservation-update/<int:pk>/', ReservationUpdateAPI.as_view()),
    path('reservation-delete/<int:pk>/', ReservationDeleteAPI.as_view()),
]