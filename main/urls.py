from django.urls import path
from .views import home, ParkingSpaceListView, ReservationListView, ParkingSpaceCreateView, ParkingSpaceUpdateView, \
                    delete_parking, ReservationCreateView, ReservationUpdateView, delete_reservation
from .api_urls import urlpatterns as api_urlpatterns

urlpatterns = [
    path('', home, name='home'),
    path('parking-lots', ParkingSpaceListView.as_view(), name='parking_spaces'),
    path('create-parking', ParkingSpaceCreateView.as_view(), name='create_parking'),
    path('update-parking/<int:id>/', ParkingSpaceUpdateView.as_view(), name='update_parking'),
    path('delete-parking/<int:id>/', delete_parking, name='delete_parking'),
    path('reservation', ReservationListView.as_view(), name='reservation'),
    path('create-reservation', ReservationCreateView.as_view(), name='create_reservation'),
    path('update-reservation/<int:id>/', ReservationUpdateView.as_view(), name='update_reservation'),
    path('delete-reservation/<int:id>/', delete_reservation, name='delete_reservation'),
] + api_urlpatterns
