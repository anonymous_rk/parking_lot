from django import forms
from .models import CustomUser
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import ValidationError


class CustomUserCreationForm(UserCreationForm, forms.ModelForm):
    class Meta:
        model = CustomUser
        fields = ("username", 'email', 'password1', 'password2')

        labels = {
            'username': 'Username',
            'email': 'Email',
            'password1': 'Password',
            'password2': 'Password confirmation',
        }

    def username_clean(self):
        username = self.cleaned_data['username'].lower()
        new = self.model.objects.filter(username=username)
        if new.count():
            raise ValidationError("User Already Exist")
        return username

    def email_clean(self):
        email = self.cleaned_data['email'].lower()
        new = self.model.objects.filter(email=email)
        if new.count():
            raise ValidationError(" Email Already Exist")
        return email

    def clean_password2(self):
        password1 = self.cleaned_data['password1']
        password2 = self.cleaned_data['password2']

        if password1 and password2 and password1 != password2:
            raise ValidationError("Password don't match")
        return password2


class CustomUserLoginForm(forms.ModelForm):

    class Meta:
        model = CustomUser
        fields = ['username', 'password']
