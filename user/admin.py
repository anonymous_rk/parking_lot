from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .forms import CustomUserCreationForm
from .models import CustomUser


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    model = CustomUser
    list_display = ['username', 'email', 'role']
    fieldsets = (
        (None, {'fields': ['username', 'password']}),
        ("Personal info", {'fields': ['first_name', 'last_name', 'email', 'role']}),
        ('Permissions', {'fields': ['is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions']}),
        ('Important dates', {'fields': ['last_login', 'date_joined']}),
    )

    add_fieldsets = (
        ('Main', {'fields': ['username', 'email', 'password1', 'password2']}),
        ('Personal info', {'fields': ['first_name', 'last_name', 'role']}),
        ('Permissions', {'fields': ['is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions']}),
    )


admin.site.register(CustomUser, CustomUserAdmin)
