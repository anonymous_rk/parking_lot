from django.urls import path, include
from .views import MyObtainTokenPairView, CustomUserAPIView
from rest_framework_simplejwt.views import TokenRefreshView
from rest_framework.routers import DefaultRouter

router = DefaultRouter()

router.register('api', CustomUserAPIView, basename='users')


urlpatterns = [
    path('', include(router.urls)),
    path('token/', MyObtainTokenPairView.as_view()),
    path('token/refresh/', TokenRefreshView.as_view()),
]