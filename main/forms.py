from django import forms
from .models import ParkingSpace, Reservation


class ParkingSpaceForm(forms.ModelForm):

    class Meta:
        model = ParkingSpace
        fields = ['created_by', 'is_reserved']

        widgets = {
            'created_by': forms.Select(attrs={
                'diabled': True
            }),
            'is_reserved': forms.CheckboxInput(attrs={
                'style': "float: left"
            })
        }

        labels = {
            'is_reserved': "Is empty?"
        }

        labels = {
            'created_by': "Lot creator",
            'is_reserved': "Is not reserved?",
        }


class ReservationForm(forms.ModelForm):

    class Meta:
        model = Reservation
        fields = ['duration', 'parking_space']

        widgets = {
            'duration': forms.TextInput(),
            'parking_space': forms.Select()
        }

        labels = {
            'parking_space': "Lot number",
            'duration': "Duration of reservation"
        }
