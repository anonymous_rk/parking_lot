from django.db import models
from user.models import CustomUser


class ParkingSpace(models.Model):
    is_reserved = models.BooleanField(default=False)
    created_by = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return 'Reserved' if self.is_reserved else 'Not reserved'


class Reservation(models.Model):
    reserved_by = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    parking_space = models.ForeignKey(ParkingSpace, on_delete=models.CASCADE)
    duration = models.CharField(max_length=50)

