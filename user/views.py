from django.shortcuts import render, redirect
from .forms import CustomUserCreationForm
from django.urls import reverse_lazy
from django.contrib.auth.views import LoginView
from django.contrib.auth import logout
from .serializers import MyTokenObtainPairSerializer, RegisterSerializer, CustomUserSerializer
from .models import CustomUser as User
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import AllowAny
from rest_framework_simplejwt.views import TokenObtainPairView
from django.http import Http404
from rest_framework.response import Response
from rest_framework import status


def register(request):
    form = CustomUserCreationForm()
    if request.method == "POST":
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('main:home')

    context = {
        'forms': form
    }
    return render(request, 'user/register.html', context)


class CustomUserLoginView(LoginView):
    template_name = 'user/login.html'
    success_url = reverse_lazy("main:home")


def logout_view(request):
    logout(request)
    return redirect('users:login')


class MyObtainTokenPairView(TokenObtainPairView):
    permission_classes = (AllowAny,)
    serializer_class = MyTokenObtainPairSerializer


class CustomUserAPIView(ModelViewSet):

    """User API CRUD"""

    queryset = User.objects.all()
    serializer_class = CustomUserSerializer

    def get_types(self, pk=None):
        try:
            return User.objects.get(pk=pk)
        except User.DoesNotExist:
            raise Http404

    def list(self, request, *args, **kwargs):
        serializer = self.serializer_class(self.queryset, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    def retrieve(self, request, pk=None, *args, **kwargs):
        query = self.get_types(pk)
        serializer = self.serializer_class(query)
        return Response(serializer.data)

    def update(self, request, pk=None, *args, **kwargs):
        query = self.get_types(pk)
        serializer = self.serializer_class(query, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    def destroy(self, request, pk=None, *args, **kwargs):
        query = self.get_types(pk)
        query.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


