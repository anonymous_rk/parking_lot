from django.db import models
from django.contrib.auth.models import AbstractUser
from .managers import CustomUserManager
from django.utils.translation import gettext_lazy as _


class CustomUser(AbstractUser):
    email = models.EmailField(_('email address'), unique=True)

    USERNAME_FIELD = 'username'

    ROLE_CHOIECES = [
        ('Manager', 'Manager'),
        ('Employee', 'Employee')
    ]

    role = models.CharField(max_length=15, choices=ROLE_CHOIECES)

    objects = CustomUserManager()

    def __str__(self):
        return self.username
