import datetime
from linecache import cache

from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, DetailView
from .models import ParkingSpace, Reservation
from .forms import ParkingSpaceForm, ReservationForm
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import PermissionDenied
from user.models import CustomUser


def home(request):
    return render(request, 'main/home.html', context={})


class ParkingSpaceListView(ListView, PermissionRequiredMixin):
    login_url = '/users/login/'
    model = ParkingSpace.objects.all()
    template_name = 'main/parkings.html'
    # permission_required = ''
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(ParkingSpaceListView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        query = self.model

        context = {
            'object_list': query,

        }
        return render(request, self.template_name, context)


class ParkingSpaceCreateView(CreateView):
    model = ParkingSpace.objects.all()
    template_name = 'main/create_parking.html'
    login_url = '/users/login/'
    form_class = ParkingSpaceForm
    # permission_required = 'organizations.add_authorganization'
    # raise_exception = True

    # def dispatch(self, request, *args, **kwargs):
    #     if not request.user.has_perm(self.permission_required):
    #         raise PermissionDenied()
    #     return super(ParkingSpaceCreatView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        form.fields['created_by'].queryset = CustomUser.objects.filter(id=request.user.id)
        context = {
            'form': form,
            'object': self.model
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = self.form_class(data=request.POST)
        if form.is_valid():
            data = form.save(commit=False)
            data.save()
            return redirect('main:parking_spaces')

        form = self.form_class(data=request.POST)
        context = {
            'form': form
        }

        return render(request, self.template_name, context)


class ParkingSpaceUpdateView(UpdateView):
    model = ParkingSpace.objects.all()
    template_name = 'main/update_parking.html'
    login_url = '/users/login/'
    form_class = ParkingSpaceForm
    # permission_required = 'organizations.add_authorganization'
    # raise_exception = True

    # def dispatch(self, request, *args, **kwargs):
    #     if not request.user.has_perm(self.permission_required):
    #         raise PermissionDenied()
    #     return super(ParkingSpaceCreatView, self).dispatch(request, *args, **kwargs)

    def get(self, request, id=None, *args, **kwargs):
        form = self.form_class(instance=ParkingSpace.objects.get(id=id))
        print(ParkingSpace.objects.get(id=id))
        context = {
            'form': form,
            'object': self.model,
            'id': id
        }
        return render(request, self.template_name, context)

    def post(self, request, id=None, *args, **kwargs):
        form = self.form_class(instance=ParkingSpace.objects.get(id=id), data=request.POST)
        print(request.POST)
        if form.is_valid():
            print(True)
            form.save()
            cache.clear()
            return redirect('main:parking_spaces')

        form = self.form_class(instance=ParkingSpace.objects.get(id=id))
        context = {
            'form': form
        }

        return render(request, self.template_name, context)


def delete_parking(request, id):
    query = get_object_or_404(ParkingSpace, id=id)
    if request.method == 'POST':
        query.delete()
        cache.clear()
        return redirect('main:parking_spaces')
    context = {'object': query, 'id': id}
    return render(request, 'main/delete_parking.html', context)


class ReservationListView(ListView, PermissionRequiredMixin):
    login_url = '/users/login/'
    model = Reservation.objects.all()
    template_name = 'main/reservation.html'
    # permission_required = ''
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm(self.permission_required):
            raise PermissionDenied()
        return super(ReservationListView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        query = self.model

        context = {
            'object_list': query,

        }
        return render(request, self.template_name, context)


class ReservationCreateView(CreateView):
    model = Reservation.objects.all()
    template_name = 'main/create_reservation.html'
    login_url = '/users/login/'
    form_class = ReservationForm
    # permission_required = 'organizations.add_authorganization'
    # raise_exception = True

    # def dispatch(self, request, *args, **kwargs):
    #     if not request.user.has_perm(self.permission_required):
    #         raise PermissionDenied()
    #     return super(ParkingSpaceCreatView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        form.fields['reserved_by'] = CustomUser.objects.get(id=request.user.id)
        form.fields['parking_space'].queryset = ParkingSpace.objects.filter(is_reserved=False).values_list('id', flat=True)
        context = {
            'form': form,
            'object': self.model
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        update_data = request.POST.copy()
        update_data.update({'reserved_by_id': request.user.id})
        form = self.form_class(data=update_data)
        if form.is_valid():
            data = form.save(commit=False)
            data.reserved_by_id = request.user.id

            data.save()
            return redirect('main:reservation')

        form = self.form_class(data=request.POST)
        context = {
            'form': form
        }

        return render(request, self.template_name, context)


class ReservationUpdateView(UpdateView):
    model = Reservation.objects.all()
    template_name = 'main/update_reservation.html'
    login_url = '/users/login/'
    form_class = ReservationForm
    # permission_required = 'organizations.add_authorganization'
    # raise_exception = True

    # def dispatch(self, request, *args, **kwargs):
    #     if not request.user.has_perm(self.permission_required):
    #         raise PermissionDenied()
    #     return super(ParkingSpaceCreatView, self).dispatch(request, *args, **kwargs)

    def get(self, request, id=None, *args, **kwargs):
        form = self.form_class(instance=Reservation.objects.get(id=id))
        form.fields['parking_space'].queryset = ParkingSpace.objects.filter(is_reserved=False).values_list('id',
                                                                                                           flat=True)
        context = {
            'form': form,
            'object': self.model,
            'id': id
        }
        return render(request, self.template_name, context)

    def post(self, request, id=None, *args, **kwargs):
        form = self.form_class(instance=Reservation.objects.get(id=id), data=request.POST)
        if form.is_valid():
            form.save()
            cache.clear()
            return redirect('main:reservation')

        form = self.form_class(instance=Reservation.objects.get(id=id))
        context = {
            'form': form
        }

        return render(request, self.template_name, context)


def delete_reservation(request, id):
    query = get_object_or_404(Reservation, id=id)
    if request.method == 'POST':
        query.delete()
        cache.clear()
        return redirect('main:reservation')
    context = {'object': query, 'id': id}
    return render(request, 'main/delete_reservation.html', context)