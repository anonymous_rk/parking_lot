# Generated by Django 4.0 on 2021-12-08 11:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='parkingspace',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
