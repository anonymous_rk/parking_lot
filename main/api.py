from rest_framework.generics import ListAPIView, CreateAPIView, UpdateAPIView, DestroyAPIView
from rest_framework.response import Response
from .serializers import ParkingSpaceSerializer, ReservationSerializer
from .models import ParkingSpace, Reservation


class ParkingSpaceListAPI(ListAPIView):
    queryset = ParkingSpace.objects.all()
    serializer_class = ParkingSpaceSerializer


class ParkingSpaceCreateAPI(CreateAPIView):
    queryset = ParkingSpace.objects.all()
    serializer_class = ParkingSpaceSerializer


class ParkingSpaceUpdateAPI(UpdateAPIView):
    queryset = ParkingSpace.objects.all()
    serializer_class = ParkingSpaceSerializer


class ParkingSpaceDeleteAPI(DestroyAPIView):
    queryset = ParkingSpace.objects.all()
    serializer_class = ParkingSpaceSerializer


class ReservationListAPI(ListAPIView):
    queryset = Reservation.objects.all()
    serializer_class = ReservationSerializer


class ReservationCreateAPI(CreateAPIView):
    queryset = Reservation.objects.all()
    serializer_class = ReservationSerializer


class ReservationUpdateAPI(UpdateAPIView):
    queryset = Reservation.objects.all()
    serializer_class = ReservationSerializer


class ReservationDeleteAPI(DestroyAPIView):
    queryset = Reservation.objects.all()
    serializer_class = ReservationSerializer
