from django.contrib import admin
from .models import ParkingSpace, Reservation

admin.site.register(ParkingSpace)
admin.site.register(Reservation)

